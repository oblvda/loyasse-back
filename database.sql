-- Active: 1674037180654@@127.0.0.1@3306@loyasse

DROP TABLE IF EXISTS post;

DROP TABLE IF EXISTS user;

DROP TABLE IF EXISTS tour;

CREATE TABLE
    tour(
        id INT PRIMARY KEY AUTO_INCREMENT,
        datetime DATETIME NOT NULL UNIQUE,
        visitorsnumber INT NOT NULL,
        email VARCHAR(255) NOT NULL
    );

CREATE TABLE
    user(
        id INT PRIMARY KEY AUTO_INCREMENT,
        email VARCHAR(255) NOT NULL UNIQUE,
        password VARCHAR(255) NOT NULL,
        role VARCHAR(255)
    );

CREATE TABLE
    post(
        id INT PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        text VARCHAR(3000) NOT NULL,
        image VARCHAR(255),
        letter VARCHAR(1) NOT NULL,
        id_user INT,
        FOREIGN KEY (id_user) REFERENCES user(id) ON DELETE SET NULL
    );

INSERT INTO
    tour (
        datetime,
        visitorsnumber,
        email
    )
VALUES (
        '2023-09-05 09:00:00.00',
        4,
        'test@test.com'
    ), (
        '2023-09-05 14:00:00.00',
        10,
        'test@testouille.com'
    ), (
        '2023-09-05 16:00:00.00',
        6,
        'test@testtt.com'
    ), (
        '2023-09-07 09:00:00.00',
        5,
        'judith@test.com'
    ), (
        '2023-09-07 11:00:00.00',
        4,
        'bonjour@bonjour.com'
    ), (
        '2023-09-07 16:00:00.00',
        12,
        'simplon@simplon.com'
    ), (
        '2023-09-06 09:00:00.00',
        9,
        'simplon@simplon.com'
    ), (
        '2023-09-06 16:00:00.00',
        5,
        'test@test.com'
    );

INSERT INTO
    user (email, password, role)
VALUES (
        'admin@email.com',
        '1234',
        'admin'
    ), (
        'test@test.com',
        '5678',
        'user'
    );

INSERT INTO
    post (
        title,
        text,
        image,
        letter,
        id_user
    )
VALUES (
        'Amédée Bonnet',
        'Chirurgien Major de l’Hôtel-Dieu de Lyon, il est connu comme le rénovateur de la chirurgie articulaire.',
        'https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Cimetière_de_Loyasse_-_Amédée_Bonnet.jpg/1200px-Cimetière_de_Loyasse_-_Amédée_Bonnet.jpg',
        'B',
        1
    ), (
        'Pierre Bossan',
        'Architecte français, il a conçu la basilique Notre-Dame de Fourvière, ainsi que l’église Saint-Georges de Lyon. La chapelle Million- Servier a été conçue selon ses plans.',
        'https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Pierre_Bossan_Loyasse.jpg/800px-Pierre_Bossan_Loyasse.jpg',
        'B',
        2
    ), (
        'Jean-François Chanoine',
        'Créateur du journal « Le Progrès » en 1859. Ce quotidien reste dans l’opposition jusqu’en 1914 et se saborde en 1942 en refusant les consignes de Vichy. Présent dans 6 départements et au travers de 15 éditions locales, il est tiré à plus de 400 000 exemplaires, pour 1 400 000 lecteurs.',
        'https://fr.geneawiki.com/images/6/63/Jean-François_CHANOINE-Loyasse.jpg',
        'C',
        1
    ), (
        'Louis-Ferdinand Ferber',
        'Pionnier de l’aviation, ses prototypes ont fait de lui un des concepteurs majeurs de l’histoire de l’aviation. En 1909, lors d’un meeting, son appareil heurte le sol. Il décèdera des suites de cet accident.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/jsLX-ACYV-qRfT-nzS2.jpg',
        'F',
        2
    ), (
        'Antoine Gailleton',
        '
Éminent professeur de dermatovénérologie, sa vie débute sur une anecdote. Sa mère lui donne naissance au beau milieu du pont du change dans la casemate des soldats en 1829. Maire de Lyon de 1881 à 1900, il restera par la suite Conseiller municipal jusqu’à sa mort. Il est surnommé «Le Père Gailleton» ou «Le Toine».',
        'https://upload.wikimedia.org/wikipedia/commons/d/d9/Cimetière_de_Loyasse_-_Tombe_Antoine_Gailleton.jpg',
        'G',
        2
    ), (
        'Sébastien Des Guidi',
        'Médecin, il introduit et permet la propagation, à partir de 1830, de l’homéopathie en France. Il est fait Chevalier de la légion d’honneur par Louis-Philippe.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/j29L-IDZf-9Meg-TNWw.jpg',
        'G',
        2
    ), (
        'Famille Guimet',
        'Jean-Baptiste Guimet (1795-1871), inven- teur du bleu d’outremer artificiel et fondateur de Péchiney. Émile Guimet (1836-1918). Passionné par les civilisations égyptiennes et asiatiques, il crée le musée éponyme de Lyon en 1879 et celui de Paris en 1889. Une partie de la collection a été transférée au Musée du Quai Branly inauguré en 2006.',
        'https://numelyo.bm-lyon.fr/f_eserv/BML:BML_01ICO0010157ee71d706c8d/preview_Source0.jpg',
        'G',
        2
    ), (
        'Edouard Herriot',
        'Maire de Lyon pendant 52 ans, Sénateur et Député du Rhône, il participa, avec le concours de l’architecte Tony Garnier, à la réalisation d’équipements qui ont contribué au rayonnement de Lyon (Hôpital Édouard Herriot, Stade et halles de Gerland.)',
        'https://numelyo.bm-lyon.fr/f_eserv/BML:BML_01ICO00101556cb442a34af/preview_Source0.jpg',
        'H',
        1
    ), (
        'Georges Hoffherr',
        'Fondateur de la «Brasserie Georges» en 1836. Dès sa création, cet établissement attire des célébrités telles qu’Emile Zola, Mistinguett et Paul Verlaine. Il existe toujours dans le quartier de Perrache.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/R9UE-0OZ0-OsiR-nS6j.jpg',
        'H',
        2
    ), (
        'Pauline-Marie Jaricot',
        'Déclarée vénérable en 1963, elle fait depuis l’objet d’un culte local. Fondatrice de l’œuvre catholique de la « Propagation de la foi », elle crée le « Rosaire vivant », chaîne de prières destinée à regrouper les Chrétiens de France. Elle s’installe au 42 Montée Saint-Barthélémy, Lyon 5è dans la «Maison de Lorette», inscrite à l’Inventaire supplémentaire des monuments historiques en 2003. Elle repose désormais à l’Eglise Saint-Nizier.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/PMO1-hlKx-ujhv-SK4I.jpg',
        'J',
        2
    ), (
        'Monument des Jouteurs',
        'Jeu très populaire, la joute de Lyon fut créée en 1807 et réunissait 33 jouteurs. Leur concession, attribuée pour mémoire, est une des premières du cimetière. Sur ce monument, subsistent différents symboles de la joute repris par de nombreux clubs de la vallée du Rhône.',
        'https://media.routard.com/image/00/7/20130816_lyon_cimetiere_loyasse_010.1383007.jpg',
        'J',
        2
    ), (
        'Jean-Espérance de Laurencin',
        'Militaire au service du Roi, il s’implique dans le réaménagement et l’urbanisation du quar- tier Perrache. Mécène érudit local, il finance une partie du projet des frères Montgolfier, qui, en remerciement, le font participer à la première ascension de leur aéronef. Homme de lettres, il entretint correspondances avec Voltaire, Rousseau et d’Alembert.',
        'https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Cimetière_de_Loyasse_-_Ancien_-_Jean_Espérance_Blandine_de_Laurencin.jpg/2560px-Cimetière_de_Loyasse_-_Ancien_-_Jean_Espérance_Blandine_de_Laurencin.jpg',
        'L',
        2
    ), (
        'Famille Leroudier',
        'Marie-Anne Leroudier née Haug (1838-1908). Artiste en broderie de renommée interna- tionale, elle enseigna à Alexis Carrel, créateur de la chirurgie vasculaire, l’utilisation des aiguilles en vue de suturer les vaisseaux sanguins. Son fils Émile Leroudier (1870-1937) fonde « La société des amis de Guignol » en 1913 et publie sous le nom de Glaudius Mathevet le célèbre recueil de pensées lyonnaises « Choses de dire et de faire ».',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/IoxB-tBYo-sxCV-39kR.jpg',
        'L',
        1
    ), (
        'Joannès Jules Marietton',
        'Avocat et homme politique socialiste, élu du 5ème arrondissement, il fut Député, adjoint au Maire, conseiller général et Vice-président de l’Assemblée départementale.',
        'https://fr.geneawiki.com/images/a/af/Joannès_Jules_MARIETTON-Loyasse.jpg',
        'M',
        2
    ), (
        'Les Pères Maristes',
        'Mouvement religieux fondé, en 1816, par douze compagnons séminaristes qui prêtèrent serment solennel d’aider leur prochain sous la protection de la Vierge Marie. La chapelle, à l’origine, concession de la famille J. Belin, a été rachetée par cette congrégation qui en a conservé les dessins, notamment celui d’un projet de pont entre les collines de Fourvière et de la Croix-Rousse.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/3kgg-q5Io-2lvT-KMMY.jpg',
        'M',
        1
    ), (
        'Claude Martin',
        '
Sa carrière militaire l’emmène aux Indes orientales où il s’établit comme Conseiller financier et politique des Nababs et des Anglais. A la tête d’une immense fortune, il est inhumé dans son Palais oriental et lègue une somme importante pour fonder des établissements d’enseignement mixte. Ainsi, seront créés les établissements de la Martinière',
        'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Cimetière_de_Loyasse_-_Louis_Martin%2C_hommage_Claude_Martin_2.jpg/1200px-Cimetière_de_Loyasse_-_Louis_Martin%2C_hommage_Claude_Martin_2.jpg',
        'M',
        1
    ), (
        'Famille Morin-Pons',
        'Famille lyonnaise de la finance, créatrice de la banque d’affaire du même nom fondée en 1805. Pierre-Gilles de Gennes, Prix Nobel de physique en 1991 et décédé en 2007, est un des descendants de cette famille.',
        'https://fr.geneawiki.com/images/1/1f/Famille_MORIN-PONS-Loyasse.jpg',
        'M',
        2
    ), (
        'Général Régis Barthélémy Mouton-Duvernet',
        'Général français ayant participé aux batailles de la Révolution et de l’Empire, distingué par l’Empereur Chevalier de la légion d’honneur et Baron d’Empire. Sa fidélité à Napoléon, notamment pendant les « Cent-Jours », lui valut sa condamnation comme « traitre au Roi ». Il sera fusillé le 27 juillet 1816, quai des Étroits.',
        'https://upload.wikimedia.org/wikipedia/commons/3/33/Cimetière_de_Loyasse_-_Tombe_Baron_Mouton-Duvernet.jpg',
        'M',
        2
    ), (
        'Les Officiers de la Révolte de 1834',
        'Concession honorifique des officiers ayant réprimé la révolte des Canuts de 1834. Cette seconde insurrection après celle de 1831, a couté la vie à plus de 600 personnes et fait 10 000 prisonniers au cours de la semaine sanglante.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/49dt-KnUl-pAbE-nW2L.jpg',
        'O',
        1
    ), (
        'Nizier Anthelme Philippe, dit Maître Philippe',
        'Célèbre thaumaturge plus connu sous le nom de Maître Philippe, il est l’une des personnalités lyonnaises les plus énigmatiques. Par la pratique de la prière, il dit agir en intermédiaire de Dieu. On lui attribue des guérisons spectaculaires qui lui valurent une reconnaissance au-delà des frontières, notamment à la cour du tsar de Russie.',
        'https://files.gandi.ws/9d/cd/9dcde21c-820a-498c-8f22-f4d30ee4bd73.jpeg',
        'P',
        1
    ), (
        'Carré des Prêtres',
        'Terrain légué par les Chanoines Antoine et Joseph Caille, il est géré par l’évêché ; sa vocation singulière est de n’accueillir que des prêtres. Selon la volonté des dona- teurs, aucun monument ne doit présenter de signe distinctif.',
        'https://numelyo.bm-lyon.fr/f_eserv/BML:BML_01ICO0010157f239a826e94/preview_Source0.jpg',
        'P',
        1
    ), (
        'Marc-Antoine Petit',
        'Membre fondateur de la Société de médecine de Lyon, chirurgien-major à l’Hôtel-Dieu, il y organisa les premiers cours d’anatomie et de clinique chirurgicale. Sa concession, de 1813, est la première du cimetière.',
        'https://upload.wikimedia.org/wikipedia/commons/9/98/Cimetière_de_Loyasse_-_Tombe_Marc-Antoine_Petit.jpg',
        'P',
        2
    ), (
        'Adélaïde Rubichon, épouse Yemeniz',
        'Elle tint le plus célèbre « salon » lyonnais de son époque à l’Hôtel de Cuzier, 30 rue Sainte-Hélène. Elle y reçut des personnages célèbres comme Lamartine et Mérimée.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/iR6h-nuzw-fYIC-19LF.jpg',
        'R',
        1
    ), (
        'Comte de Saint-Priest',
        'Diplomate reconnu pour ses relations avec l’Empire Ottoman, il fut un homme de cour et ministre de Louis XVI. Ses mémoires témoignent du climat social et politique de la fin du XVIII ème siècle.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/vyUU-AHro-IvQk-H7XC.jpg',
        'S',
        2
    ), (
        'Paul de Vive, dit Velocio',
        'Stéphanois d’adoption, il invente le dérailleur. Il fonde la première course cycliste de la région en 1882 et est à l’origine de la Fédération française de cyclotourisme. Grâce à lui, Saint-Etienne est la capitale du cyclisme et lui rend hommage chaque année.',
        'https://storage.googleapis.com/www.ar-tour.com/users/25d1cf49-8ab3-490b-b710-bd0fb86e32b0/images/temp/main/X8Sq-J11N-HSGd-pnMg.jpg',
        'V',
        2
    ), (
        'Jean-Baptiste Willermoz',
        'Grand bourgeois et illustre personnage de l’histoire maçonnique, Maître fabricant d’étoffes de soies et commissionnaire en soierie.',
        'https://cdn-s-www.leprogres.fr/images/48dbe675-2ace-4840-a379-e8146643f874/NW_raw/photo-progres-joel-philippon-1604074337.jpg',
        'W',
        1
    );