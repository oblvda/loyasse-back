<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthController extends AbstractController
{

    public function __construct(private UserRepository $repo)
    {
    }

    #[Route('/api/user', methods: 'POST')]
    public function index(UserRepository $repo, Request $request, SerializerInterface $serializer, UserPasswordHasherInterface $hasher, ValidatorInterface $validator): JsonResponse
    {
        try {
            $user = $serializer->deserialize($request->getContent(), User::class, 'json');

        } catch (\Exception $e) {
            return $this->json('Invalid body', 400);
        }

        $errors = $validator->validate($user);
        if($errors->count() > 0) {
            return $this->json(['errors' => $errors], 400);
        }

        if ($repo->findByEmail($user->getEmail())) {
            return $this->json('User Already exists', 400);
        }

        $hash = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hash);
        $user->setRole('ROLE_USER'); //On lui assigne le role user par défaut

        $repo->persist($user);

        return $this->json($user, 201);
    }

    #[Route('/api/protected', methods: 'GET')]
    public function one() {
        return $this->json($this->getUser());
    }

    #[Route('/api/protected', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $user = $this->repo->findById($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }
        try {
            $toUpdate = $serializer->deserialize($request->getContent(), User::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);

            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
}