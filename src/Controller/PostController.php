<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/post')]
class PostController extends AbstractController
{
    private PostRepository $repo;
    public function __construct(PostRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all()
    {
        $posts = $this->repo->findAll();
        return $this->json($posts);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $post = $this->repo->findById($id);
        if (!$post) {
            throw new NotFoundHttpException();

        }
        return $this->json($post);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        try {
            $post = $serializer->deserialize($request->getContent(), Post::class, 'json');
            
            $this->repo->persist($post);


            return $this->json($post, Response::HTTP_CREATED);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }

    }

    #[Route('/{id}', methods:'DELETE')]
    public function remove(int $id){
        $post = $this->repo->findById($id);
        if(!$post){
            throw new NotFoundHttpException();
        }

        $this->repo->delete($post);
        return new JsonResponse(null, 204);
    }

    #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer, ValidatorInterface $validator) {   
        $post = $this->repo->findById($id);
        if (!$post) {
            throw new NotFoundHttpException();
        }
        try {


            $toUpdate = $serializer->deserialize($request->getContent(), Post::class, 'json');
            $toUpdate->setId($id);
            $this->repo->update($toUpdate);

            return $this->json($toUpdate);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
        
    }

    #[Route('/letter/{letter}', methods: 'GET')]
    public function oneByLetter(string $letter)
    {
        $posts = $this->repo->findByLetter($letter);
        return $this->json($posts);
    }
}