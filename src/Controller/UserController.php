<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/api/user')]
class UserController extends AbstractController
{
    public function __construct(private UserRepository $repo)
    {
    }

    // #[Route(methods: 'GET')]
    // public function all(): JsonResponse
    // {
    //     return $this->json($this->repo->findAll());
    // }

    // #[Route('/{id}', methods: 'GET')]
    // public function one(int $id)
    // {
    //     $user = $this->repo->findById($id);
    //     if (!$user) {
    //         throw new NotFoundHttpException();
    //     }
    //     return $this->json($user);
    // }

    #[Route('/api/user/{id}/promote', methods: 'PATCH')]
    public function promote(int $id, Request $request): JsonResponse
    {
        $user = $this->repo->findById($id);
        if (!$user) {
            throw new NotFoundHttpException('User does not exist');
        }
        $user->setRole('ROLE_ADMIN');
        $this->repo->update($user);

        return $this->json($user);
    }

}