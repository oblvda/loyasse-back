<?php 

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Post {
	#[Assert\NotBlank]
    private ?int $id;
	#[Assert\NotBlank]
    private ?string $title;
	#[Assert\NotBlank]
    private ?string $text; 
    private ?string $image;
	#[Assert\NotBlank]
	private ?string $letter;
	private ?int $idUser;

	public function __construct(?string $title = null, ?string $text = null, ?string $image = null, ?string $letter = null, ?int $idUser = null, ?int $id = null) {
		$this->title = $title;
		$this->text = $text;
		$this->image = $image;
		$this->letter = $letter;
		$this->idUser = $idUser;
		$this->id = $id;
	}

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getTitle(): ?string {
		return $this->title;
	}
	
	/**
	 * @param  $title 
	 * @return self
	 */
	public function setTitle(?string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getText(): ?string {
		return $this->text;
	}
	
	/**
	 * @param  $text 
	 * @return self
	 */
	public function setText(?string $text): self {
		$this->text = $text;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getImage(): ?string {
		return $this->image;
	}
	
	/**
	 * @param  $image 
	 * @return self
	 */
	public function setImage(?string $image): self {
		$this->image = $image;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getLetter(): ?string {
		return $this->letter;
	}
	
	/**
	 * @param  $letter 
	 * @return self
	 */
	public function setLetter(?string $letter): self {
		$this->letter = $letter;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getIdUser(): ?string {
		return $this->idUser;
	}
	
	/**
	 * @param  $idUser 
	 * @return self
	 */
	public function setIdUser(?string $idUser): self {
		$this->idUser = $idUser;
		return $this;
	}
}