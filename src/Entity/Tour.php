<?php 

namespace App\Entity;
use DateTime; 
use Symfony\Component\Validator\Constraints as Assert;

class Tour {
	#[Assert\NotBlank]
    private ?int $id; 
	#[Assert\NotBlank]
	#[Assert\Range(
        min: 'now'
    )]
	#[Assert\Unique]
    private ?DateTime $dateTime;
	#[Assert\NotBlank]
	#[Assert\Range(
		min: 4,
		max: 12,
		notInRangeMessage: 'You must check between 4 and 12 visitors')]
    private ?int $visitorsNumber;
	#[Assert\NotBlank]
	private ?string $email;

	public function __construct(?DateTime $dateTime = null, ?int $visitorsNumber = null, ?string $email = null, ?int $id = null) {
		$this->dateTime = $dateTime;
		$this->visitorsNumber = $visitorsNumber;
		$this->email = $email;
		$this->id = $id;
	}

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDateTime(): ?DateTime {
		return $this->dateTime;
	}
	
	/**
	 * @param  $dateTime 
	 * @return self
	 */
	public function setDateTime(?DateTime $dateTime): self {
		$this->dateTime = $dateTime;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getVisitorsNumber(): ?int {
		return $this->visitorsNumber;
	}
	
	/**
	 * @param  $visitorsNumber 
	 * @return self
	 */
	public function setVisitorsNumber(?int $visitorsNumber): self {
		$this->visitorsNumber = $visitorsNumber;
		return $this;
	}

	/**
	 * @return 
	 */
	public function getEmail(): ?string {
		return $this->email;
	}
	
	/**
	 * @param  $email 
	 * @return self
	 */
	public function setEmail(?string $email): self {
		$this->email = $email;
		return $this;
	}
}