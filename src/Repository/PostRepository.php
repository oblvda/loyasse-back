<?php

namespace App\Repository;

use App\Entity\Post;
use PDO;

class PostRepository
{

    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::getConnection();
    }


    // Fonction qui appelle les infos de la base de données
    private function sqlToPost(array $line): Post
    {
        return new Post($line['title'], $line['text'], $line['image'], $line['letter'], $line['id']);
    }

    // Fonction qui permet d'afficher tous les articles
    public function findAll(): array
    {
        $post = [];


        $statement = $this->connection->prepare('SELECT * FROM post');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $post[] = new Post($item['title'], $item['text'], $item['image'], $item['letter'], $item['id_user'], $item['id']);
        }
        return $post;
    }

    // Fonction qui permet d'afficher un seul article
    public function findById(int $id)
    {
        $statement = $this->connection->prepare("SELECT * FROM post WHERE id=:id");
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            return $this->sqlToPost($result);
        }
        return null;
    }

    // Fonction qui permet d'ajouter un article
    public function persist(Post $post): void
    {
        $connection = Database::getConnection();
        $statement = $connection->prepare('INSERT INTO post (title, text, image, letter, id_user) VALUES (:title, :text, :image, :letter, :id_user)');
        $statement->bindValue(':title', $post->getTitle());
        $statement->bindValue(':text', $post->getText());
        $statement->bindValue(':image', $post->getImage());
        $statement->bindValue(':letter', $post->getLetter());
        $statement->bindValue(':id_user', $post->getIdUser());

        $statement->execute();

        $post->setId($connection->lastInsertId());
    }

    // Fonction qui permet de supprimer un article
    public function delete(Post $post)
    {
        $statement = $this->connection->prepare("DELETE FROM post WHERE id=:id");
        $statement->bindValue('id', $post->getId());

        $statement->execute();
    }

    // Fonction qui permet d'éditer un article
    public function update(Post $post)
    {
        $statement = $this->connection->prepare('UPDATE post SET title=:title, text=:text, image=:image, letter=:letter, id_user=:id_user WHERE id=:id');

        $statement->bindValue(':title', $post->getTitle());
        $statement->bindValue(':text', $post->getText());
        $statement->bindValue(':image', $post->getImage());
        $statement->bindValue(':letter', $post->getLetter());
        $statement->bindValue(':id_user', $post->getIdUser());
        $statement->bindValue(':id', $post->getId());


        $statement->execute();
    }

    // Fonction qui permet de trouver un post par sa lettre
    public function findByLetter(string $letter)
    {
        $post = [];

        $statement = $this->connection->prepare("SELECT * FROM post WHERE letter=:letter");
        $statement->bindValue('letter', $letter);

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $post[] = new Post($item['title'], $item['text'], $item['image'], $item['letter'], $item['id_user'], $item['id']);
        }
        return $post;
    }
}