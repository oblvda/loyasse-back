<?php

namespace App\Repository;

use App\Entity\Tour;
use PDO;

class TourRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::getConnection();
    }


    // Fonction qui appelle les infos de la base de données
    private function sqlToTour(array $line): Tour
    {
        return new Tour(new \DateTime ($line['datetime']), $line['visitorsnumber'], $line['email'], $line['id']);
    }

    // Fonction qui permet d'afficher toutes les visites
    public function findAll(): array
    {
        $tour = [];


        $statement = $this->connection->prepare('SELECT * FROM tour');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $tour[] = $this->sqlToTour($item);
        }
        return $tour;
    }

    // Fonction qui permet d'afficher une seule visite
    public function findById(int $id)
    {
        $statement = $this->connection->prepare("SELECT * FROM tour WHERE id=:id");
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            return $this->sqlToTour($result);
        }
        return null;
    }

    // Fonction qui permet d'ajouter une visite
    public function persist(Tour $tour): void
    {
        $connection = Database::getConnection();
        $statement = $connection->prepare('INSERT INTO tour (datetime, visitorsnumber, email) VALUES (:datetime, :visitorsnumber, :email)');
        $statement->bindValue(':datetime', $tour->getDateTime()->format('Y-m-d H:i:s'));
        $statement->bindValue(':visitorsnumber', $tour->getVisitorsNumber());
        $statement->bindValue(':email', $tour->getEmail());

        $statement->execute();

        $tour->setId($connection->lastInsertId());
    }

    // Fonction qui permet de supprimer une visite
    public function delete(Tour $tour)
    {
        $statement = $this->connection->prepare("DELETE FROM tour WHERE id=:id");
        $statement->bindValue('id', $tour->getId());

        $statement->execute();
    }

    // Fonction qui permet d'éditer une visite
    public function update(Tour $tour)
    {
        $statement = $this->connection->prepare('UPDATE tour SET datetime=:datetime, visitorsnumber=:visitorsnumber, email=:email WHERE id=:id');

        $statement->bindValue(':datetime', $tour->getDateTime()->format('Y-m-d H:i:s'));
        $statement->bindValue(':visitorsnumber', $tour->getVisitorsNumber());
        $statement->bindValue(':email', $tour->getEmail());
        $statement->bindValue(':id', $tour->getId());

        $statement->execute();
    }
}